﻿const path = require('path');
const webpack = require('webpack');
const AotPlugin = require('@ngtools/webpack').AotPlugin;
const CheckerPlugin = require('awesome-typescript-loader').CheckerPlugin;
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = (env) => {
    const isDevBuild = !(env && env.prod);

    const extractCss = new ExtractTextPlugin({filename: 'site.css'});

    const plugins = [
        new CheckerPlugin(),
        new webpack.DefinePlugin({
            PROD_ENV: !isDevBuild
       }),
       extractCss,
       new AotPlugin({
            tsConfigPath: './tsconfig.json',                            
            entryModule: 'src/modules/main/main.module#MainModule'                
         })       
    ];
    
    const config = {
        resolve: { extensions: [ '.js', '.ts'] },
        entry:{
            "app": ["./src/main.ts",
            "./src/styles/styles.scss"]
        
        },
        output:{
            path: path.join(__dirname,"../Content/dist"),
            // ленивая загрузка
            publicPath:  "../Content/dist/",            
            chunkFilename: '[id].[hash].chunk.js',
            //ленивая загрузка
            filename: "[name].bundle.js"
        },
        module: {
            rules:[
                {
                    test: /\.ts$/,                    
                    loader: '@ngtools/webpack'
                },

                { test: /\.html$/, use: 'html-loader?minimize=false' },

                

                { test: /\.s?css$/, exclude: path.join(__dirname,"src", "styles"),  use: ["to-string-loader",  isDevBuild? "css-loader": "css-loader?minimize", "sass-loader"]},
                { 
                    test: /\.s?css$/, 
                    include: path.join(__dirname,"src", "styles"),             
                    use: extractCss.extract({
                        fallback: "style-loader",                        
                        use: [ {
                            loader: isDevBuild? "css-loader": "css-loader?minimize"
                        }, {
                            loader: "sass-loader"
                        }  ]
                    }) 
                },
            ]
        },
        plugins: isDevBuild ? plugins : plugins.concat([
            new webpack.optimize.UglifyJsPlugin()            
        ]),
        devtool: 'inline-source-map'     

    };

    return config;
};
