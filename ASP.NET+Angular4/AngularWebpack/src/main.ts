import 'reflect-metadata';
import "./polyfills"
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { MainModule } from './modules/main/main.module';
import { enableProdMode } from '@angular/core';

declare const PROD_ENV:boolean;

if(PROD_ENV){    
    enableProdMode();    
}
const modulePromise = platformBrowserDynamic().bootstrapModule(MainModule);