import { Component } from "@angular/core";


@Component({
    selector: "lazy",
    templateUrl: "./main.component.html",
    styleUrls: ["./main.component.css"]
})
export class MainComponent {}