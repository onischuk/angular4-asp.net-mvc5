import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MainComponent } from "./components/main/main.component";
import { LazyRoutingModule } from "./lazy-routing.module";



@NgModule({
    imports:[
        CommonModule,
        LazyRoutingModule
    ],
    declarations:[
        MainComponent
    ]
})
export class LazyModule{}