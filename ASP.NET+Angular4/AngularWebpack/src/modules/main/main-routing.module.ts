import {RouterModule, Routes, PreloadAllModules} from '@angular/router';
import { NgModule } from "@angular/core";
import { AppComponent } from './components/app/app.component';

const routes: Routes = [
    
    { path: "lazy", loadChildren: "../lazy/lazy.module#LazyModule"},
    { path: "", component: AppComponent},
    { path: "**" , redirectTo: "/"}
];


@NgModule({
    imports:[RouterModule.forRoot(routes,{preloadingStrategy: PreloadAllModules})],
    exports:[RouterModule]
})
export class MainRoutingModule{}