import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser"
import { MainComponent } from "./components/main/main.component";
import { MainRoutingModule } from "./main-routing.module";
import { AppComponent } from "./components/app/app.component";


@NgModule({
    imports:[
        BrowserModule,
        MainRoutingModule
    ],
    declarations:[
        MainComponent,
        AppComponent
    ],
    bootstrap: [MainComponent]
})
export class MainModule {}